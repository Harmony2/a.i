def is_palindrome(word):
    print(word[0], word[len(word)-1])
    if len(word) <= 1:
        return True
    elif word[0] == word[len(word)-1]:
        return is_palindrome(word[1:len(word)-1])
    else:
        return False

print(is_palindrome("rotor"))