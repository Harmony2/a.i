#include <iostream>
#include <ctime>
#include <cstdlib>
#include <vector>
using namespace std;

vector <int> swap(vector <int> vec, int a, int b){
	int holder = vec[a];
	vec[a] = vec[b];
	vec[b] = holder;
	return vec;
}

int find_smallest(vector <int> vec, int min_index){
	int min_val = vec[min_index];
	for(int i = min_index; i<vec.size(); i++){
		if(vec[i] < min_val){
			min_val = vec[i];
			min_index = i;
		}
	}
	return min_index;
}

vector <int> generate_vector(int n, vector <int> vec){
	srand(time(0));
	for(int i=0; i<n; i++){
		vec.push_back(rand()%1000);
	}
	return vec;
}

vector <int> selection_sort(vector <int> vec){
	for(int i=0; i<vec.size(); i++){
		vec = swap(vec, i, find_smallest(vec, i));
	}
	return vec;
}

main(){
	vector <int> a;
	a = generate_vector(100, a);
	
	for(int i=0; i<a.size();i++){
		cout << a[i] << " ";
	}
	
	a = selection_sort(a);
	cout << "" << endl;
	for(int i=0; i<a.size();i++){
		cout << a[i] << " ";
	}
}

