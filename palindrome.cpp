#include <iostream>
using namespace std;

int is_palindrome(string word){
	if(word.length() <= 1){
		return 1;
	}
	
	cout << word[0] << word[word.size()-1] << endl;
	
	if(word[0] == word[word.size()-1]){
		return is_palindrome(word.substr(1, word.length()-2));
	}
	else{
		return 0;
	}
	
}

int main(){
	cout << is_palindrome("babbab") << endl;
}
