import queue


#Store graph as adjacency list
adjacency_list = [
    [1],
    [0,4,5],
    [3,4],
    [2,6],
    [1,2],
    [1,6],
    [5,3],
    []
]

#Search function
def search(graph, initial_pos):
    #create dict to hold position's information
    #       node    predecessor, cost
    info = {initial_pos:[None, 0]}
    
    #start queue and enqueue initial position
    q = queue.Queue()
    q.put(initial_pos)

    #while queue is not empty
    while q.empty() == False:
        #node = dequeue (get first of queue)
        node = q.get()
        #for each outdegree of node
        for i in graph[node]:
            if i not in info.keys():
                # append neighbor with node as predecessor and cost = node's cost + 1
                info[i] = [node, (info[node][1]+1)]
                # enqueue i
                q.put(i)
    return info

def smallest_cost(info, objective, initial):
    if objective not in info.keys():
        return False

    #create path list and append objective's predecessor
    path = []
    path.append(info[objective][0])

    #for i in range 0, objective's path cost, append a path from the objective's predecessor to the starting position
    for i in range(0, info[objective][1]-1):
        path.append(info[path[i]][0])

    #invert list and append objective position
    path = path[::-1]
    path.append(objective)
    return path

#set starting position and objective
start_pos = 0
objective = 7

#call functions with starting position and objective
info = search(adjacency_list, start_pos)
path = smallest_cost(info, objective, start_pos)

for i in info:
    print("Node = %s\nDistance = %s\nPredecessor = %s\n"%(i, info[i][1], info[i][0]))
print("smallest cost path = %s"%path)