import random
import time
import math

def gen_num(n):
    nums = [i for i in range(0,n+1)]
    x = random.randint(0, n)
    return nums, x


#m = min
#n = max
#nums = array
#c = counter
def search(m, n, x, nums, c):
    #med = (int((n - m)*0.5)) + m
    med = int((n+m)*0.5)
    med = int(med)
    if nums[med] == x:
        print("Found it is %s tries! %s"%(c, med))
        return True
    elif nums[med] < x:
        m = (med + 1)
    elif nums[med] > x:
        n = (med - 1)
    print("m, n = %s, %s"%(m, n))
    c += 1
    return search(m, n, x, nums, c)

start = time.time()

n = 1000
nums, x = gen_num(n)
search(0, n, x, nums, 0)

end = time.time()
print("the max tries should be %d"%math.log2(n))
print(end - start)