#include <iostream>
#include <ctime>
#include <cstdlib>
#include <vector>
using namespace std;

vector <int> generate_array(int n){
	vector<int> nums;
	for (int i=0; i<=n; i++){
		nums.push_back(i);
	}
	return nums;
}

int get_random(int n){
	srand(time(0));
	int t = rand()%10000000;
}

int linear_search(vector <int> nums, int x){
	for(int i=0; i<=nums.size(); i++){
		if(x == nums[i]){
			return i;
		}
	}
	return 0;
}

int recursive_binary_search(vector <int> nums, int x, int m, int n){
	int guess = (n+m)/2;
	if(nums[guess] == x){
		return guess;
	}
	else if(nums[guess] > x){
		n = guess - 1;
	}
	else{
		m = guess + 1;
	}
	//cout << m << " " << n << endl;
	return recursive_binary_search(nums, x, m, n);
}

int binary_search(vector <int> nums, int x){
	int m = 0;
	int n = nums.size();
	while (n >= m){
		int guess = (n+m)/2;
		if(nums[guess] == x){
			return guess;
		}
		else if(nums[guess] > x){
			n = guess - 1;
		}
		else{
			m = guess + 1;
		}
		//cout << m << " " << n << endl;
	}
}

main(){
	int n = 1000000000;
	
	vector <int> nums = generate_array(n);
	cout << nums[nums.size()-1] << endl;
	int x = n;
	int m = 0;
	//cout << x << endl;
	
	cout << "linear " << linear_search(nums, x) << endl;		
	//cout << "recursive binary " << recursive_binary_search(nums, x, m, n) << endl;
	//cout << "binary " << binary_search(nums, x) << endl;
}
