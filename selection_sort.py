def find_min_value_index(a, min_index):
    min_val = a[min_index]
    for i in range(min_index+1, len(a)):
        if  min_val > a[i]:
            min_index = i
            min_val = a[i]
    return min_index

def swap_array_elements(a, x, y):
    holder = a[x]
    a[x] = a[y]
    a[y] = holder
    return a

def selection_sort(a):
    for i in range(len(a)):
        min = find_min_value_index(a, i)
        swap_array_elements(a, i, min)
    return a

a = [10, 19, 23, 11, 4, 2, 15, 2, 22, 1, 1, 1, 1]
print(a)
selection_sort(a)
print(a)