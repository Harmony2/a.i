import queue
import time

adjacency_list = [
    #[outdegree node, cost]
    [[1,1]],    #0
    [[0,1],[4,1],[5,5]],    #1
    [[3,10],[4,1]],  #2
    [[2,1],[6,8]],  #3
    [[1,1],[2,1]],  #4
    [[1,1],[6,3]],  #5
    [[5,1],[3,1]],  #6
    []
]

def search(graph, initial_pos):
    info = {initial_pos:[None, 0, False]}
    
    #start queue and enqueue initial position
    q = queue.Queue()
    q.put(initial_pos)

    #while queue is not empty
    while q.empty() == False:
        node = q.get()
        print("getting %s\n"%node)
        for out in graph[node]:
            if out[0] not in info.keys():
                info[out[0]] = [node, info[node][1] + out[1], False]
        info[node][2] = True
        print(info)
        smallest_cost = None
        next_node = None
        for i in info.keys():
            print("\nchecking ", i)
            print("cost:", info[i][1])
            print("current cost: %s"%smallest_cost)
            if (smallest_cost == None) & (info[i][2] == False):
                smallest_cost = info[i][1]
                next_node = i
                print("\nnew cost: %s"%smallest_cost)
                print("new node: %s"%next_node)
            if smallest_cost == None:
                pass
            elif (smallest_cost > info[i][1]) & (info[i][2] == False):
                smallest_cost = info[i][1]
                next_node = i
                print("\nnew cost: %s"%smallest_cost)
                print("new node: %s"%next_node)
            # print("next node: %s\nsmallest cost: %s"%(next_node, smallest_cost))
        if next_node != None:
            q.put(next_node)
    return info


def smallest_cost(info, objective, initial):
    if objective not in info.keys():
        return False

    #create path list and append objective's predecessor
    path = []
    path.append(info[objective][0])
    pred = info[objective][0]
    
    while True:
        pred = info[pred][0]
        if pred == None:
            break
        path.append(pred)

    #invert list and append objective position
    path = path[::-1]
    path.append(objective)
    cost = info[objective][1]
    return path, cost

start_pos = 0
objective = 6

info = search(adjacency_list, start_pos)
path, cost = smallest_cost(info, objective, start_pos)

for i in info.keys():
    print("Node = %s\nDistance = %s\nPredecessor = %s\n"%(i, info[i][1], info[i][0]))
print("path = %s\ncost = %s"%(path, cost))